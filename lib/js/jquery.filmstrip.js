!function ($) {
    "use strict";
    var Filmstrip = function (element, options) {
        var that = this;
        this.$element = $(element);
        this.options = options;
        this.options.slide && this.slide(this.options.slide);
        this.options.pause == 'hover' && this.$element
            .on('mouseenter', $.proxy(this.pause, this));
        var $ctrl = that.$element.find('.filmstrip-control');
        this.$element.find('.filmstrip-top').hover(function() {
            $ctrl.stop();
            $ctrl.fadeIn(200);
        }, function() {
            $ctrl.stop();
            $ctrl.fadeOut(200);
        });
        $ctrl.hover(function() {
            $(this).stop();
            $(this).css({opacity:0.9});
        }, function() {
            $(this).css({opacity:0.5});
        });
    };
    Filmstrip.prototype = {
        to: function (pos) {
            var $active = this.$element.find('.active')
              , children = $active.parent().children()
              , activePos = children.index($active)
              , that = this;
            if (pos > (children.length - 1) || pos < 0)
                return;
            if (this.sliding) {
                return this.$element.one('slid', function () {
                    that.to(pos);
                });
            }
            return this.slide(pos > activePos ? 'next' : 'prev', $(children[pos]));
        }
        ,pause: function (e) {
            if (!e)
                this.paused = true;
            clearInterval(this.interval);
            this.interval = null;
            return this;
        },
        next: function () {
            if (this.sliding)
                return;
            return this.slide('next');
        },
        prev: function () {
            if (this.sliding)
                return;
            return this.slide('prev');
        },
        selector: function (pos) {
            if (this.sliding)
                return;
            return this.to(pos-1);
        },
        slide: function (type, next) {
            var $active = this.$element.find('.active')
              , $next = next || $active[type]()
              , isCycling = this.interval
              , fallback  = type == 'next' ? 'first' : 'last'
              , that = this
              , e = $.Event('slide');
            $next = $next.length ? $next : this.$element.find('.item')[fallback]();
            var index = $next.parent().children().index($next)
              , $selector = this.$element.find('.selector')
              , $thumb = $selector.parent().children().eq(index)
              , $thumbs = $selector.parent()
              , $last = $thumbs.children().last()
              , thumbsWidth = $last.position().left + $last.width() + $selector.outerWidth() - $selector.width();
            $selector.removeClass('selector');
            $thumb.addClass('selector');
            $selector.find('.pointer').appendTo($thumb);
            if (thumbsWidth > this.$element.width()) {
                var thumbsPos = this.$element.width() / 2 - $thumb.position().left - $thumb.width() / 2;

                if (thumbsPos > 0) {
                    thumbsPos = 0;
                } else if (thumbsPos <= this.$element.width() - thumbsWidth) {
                    thumbsPos = this.$element.width() - thumbsWidth;
                }
                $thumbs.animate({left:thumbsPos}, 500, 'swing');
            }
            if ($next.hasClass('active'))
                return;
            this.$element.trigger(e);
            if (e.isDefaultPrevented())
                return;
            $active.fadeOut(500, function() {
                $active.removeClass('active');
                $next.addClass('active');
                that.sliding = false;
                that.$element.trigger('slid');
            });
            return this;
        }
    };
    $.fn.filmstrip = function (option) {
        return this.each(function () {
            var $this = $(this)
              , data = $this.data('filmstrip')
              , options = $.extend({}, $.fn.filmstrip.defaults, typeof option == 'object' && option);
            if (!data)
                $this.data('filmstrip', (data = new Filmstrip(this, options)));
            if (typeof option == 'number')
                data.to(option);
            else if (options.filmstrip == 'selector')
                data.selector(options.index);
            else if (typeof option == 'string' || (option = options.filmstrip))
                data[option]();
        });
    };
    $.fn.filmstrip.Constructor = Filmstrip;
    $(function () {
        $('body').on('click.filmstrip.data-api', '[data-filmstrip]', function ( e ) {
            var $this = $(this), href
              , $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
              , options = !$target.data('modal') && $.extend({}, $target.data(), $this.data());
            if (options.filmstrip == 'selector') {
                var temp=$this.parent().children().index($this);
                options.index = temp;
            }
            $target.filmstrip(options);
            e.preventDefault();
        })
    })
}(window.jQuery);